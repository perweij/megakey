#!/usr/bin/env python3
# ##################################################
# megakey - un-interceptable keyboard macros
#
#
# HISTORY:
#
# * DATE - v0.0.1  - First Creation
#
#
# Copyright Per Weijnitz <per.weijnitz@gmail.com>
#
# Licence: GPL v3
# ##################################################
import subprocess
import json
import sys

version = "0.0.1"
debug = True


# Parse config
conffile = sys.argv[1]
config = json.load(open(conffile))

# Rudimentary check of config file
if not config["kbd_id"] or not config["macros"]:
    sys.exit("**** config file seems bad")

# Determine the longest defined key sequence in the config file
maxlen = max(len(x["seq"]) for x in config["macros"])


# Given two lists l0 and l1, return True if l1 is a suffix of l0.
def lists_equal_suffix(l0, l1):
    if len(l0) < len(l1):
        return False
    offset = len(l0) - len(l1)
    for i in range(len(l1)):
        if l0[i + offset] != l1[i]:
            return False
    return True


# Main
process = subprocess.Popen(["xinput", "test", str(config["kbd_id"])], stdout=subprocess.PIPE)
#process = subprocess.Popen(["stdbuf", "-oL", "-eL", "/tmp/xinputtest"], stdout=subprocess.PIPE)

queue = []
for line in iter(process.stdout.readline, ''):
    if not line:
        break
    # Push next data line to event queue
    print("parsing " + line.decode().strip(), file=sys.stderr)
    _, event, code = line.decode().strip().split()
    queue.extend([event, code])

    # Trim queue length
    if len(queue) > maxlen:
        queue = queue[-maxlen:]

    # Match queue to the defined key sequences
    for macro in config["macros"]:
        if lists_equal_suffix(queue, macro["seq"]):
            print("running " + macro["cmd"], file=sys.stderr)
            subprocess.Popen(macro["cmd"].split())
            queue = queue[len(macro["seq"]):]
            break

process.stdout.close()
process.wait()
