#!/usr/bin/perl -w
# ##################################################
# megakey - un-interceptable keyboard macros
#
#
# HISTORY:
#
# * DATE - v0.0.1  - First Creation
#
#
# Copyright Per Weijnitz <per.weijnitz@gmail.com>
#
# Licence: GPL v3
# ##################################################
use strict;
use warnings;
use Config::General qw(ParseConfig);


my $version="0.0.1";

my $debug = 1;


# Parse config
my $conffile = shift(@ARGV);
-f $conffile || die("provide config");
my %config = ParseConfig($conffile);

# Rudimentary check of config file
die("**** config file seems bad")
    if(! defined($config{kbd_id}) || ! defined($config{macro}) || scalar @{$config{macro}} == 0);

# Determine the longest defined key sequence in the config file
my $maxlen = (sort { $b <=> $a } map { scalar @{$_->{'seq'}} } @{$config{macro}})[0];


# Given two array references l0 and l1, return 1 if l1 is a suffix of l0.
# Equality operator used: ne
sub lists_equal_suffix {
    my $l0 = shift;
    my $l1 = shift;

    if($#{$l0} < $#{$l1}) {
	return 0;
    }
    my $offset = $#{$l0} - $#{$l1};
    for(my $i = 0; $i <= $#{$l1}; $i++) {
	if($l0->[$i + $offset] ne $l1->[$i]) {
	    return 0;
	}
    }
    return 1;
}



# Main
open(my $fh, '-|', "xinput test $config{kbd_id}") || die("could not run: xinput test $config{kbd_id}");
<$fh>; # the first line is usually noise from starting the script
my @queue=();
while (<$fh>) {
    # Push next data line to event queue
    my ($what,$event,$code) = split;
    push(@queue, $what,$event,$code);

    # Trim queue length
    if(@queue > $maxlen) {
	splice(@queue, 0, @queue - $maxlen);
    }

    # Match queue to the defined key sequences
    map {
	print STDERR "macro:\t".join(" ",@{$_->{'seq'}}),"\nqueue:\t",join(" ",@queue),"\n\n"
	    if($debug);
	
	if(lists_equal_suffix(\@queue, $_->{'seq'})) {
	    print STDERR "running $_->{cmd}\n";
	    system($_->{'cmd'}.' &');
	    splice(@queue, 0, scalar @{$_->{'seq'}});
	}
    } @{$config{macro}};
}
close($fh);
